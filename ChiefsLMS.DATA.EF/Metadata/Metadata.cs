﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ChiefsLMS.DATA.EF//.Metadata
{
    #region DivisionMetadata

    
    public class DivisionMetadata
    {
        //public int DivisionID { get; set; }
        
        [Display(Name = "Division")]
        [Required(ErrorMessage = "Division Name is required.")]
        public string DivisionName { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        [Display(Name = "Active")]
        [Required(ErrorMessage = "Active is required.")]
        public bool IsActive { get; set; }

    }

    [MetadataType(typeof(DivisionMetadata))]
    public partial class Division { }

    #endregion

    #region TeamMetadata

    public class TeamMetadata
    {
        //public int TeamID { get; set; }
        [Display(Name = "Team Name")]
        [Required(ErrorMessage = "Active is required.")]
        public string TeamName { get; set; }

        [Display(Name = "Division")]
        [Required(ErrorMessage = "Active is required.")]
        public int DivisionID { get; set; }

        [Required(ErrorMessage = "Active is required.")]
        public string Introduction { get; set; }

        [Display(Name = "Video URL")]
        public string VideoURL { get; set; }

        [Display(Name = "Schedule")]
        public string PdfFileName { get; set; }

        [Display(Name = "Active")]
        [Required(ErrorMessage = "Active is required.")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(TeamMetadata))]
    public partial class Team { }

    #endregion

    #region CourseCompletionMetadata

    public class CourseCompletionMetadata
    {
        //public int DivisionCompletionID { get; set; }
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "User ID is required.")]
        public string UserID { get; set; }

        [Display(Name = "Division")]
        [Required(ErrorMessage = "Division is required.")]
        public int DivisionID { get; set; }

        [Display(Name = "Date Completed")]
        [Required(ErrorMessage = "Date Completed is required.")]
        public DateTime DateCompleted { get; set; }
    }

    [MetadataType(typeof(CourseCompletionMetadata))]
    public partial class CourseCompletion { }
    #endregion

    #region UserDetailMetadata

    public class UserDetailMetadata
    {
        //public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    #endregion

    #region LessonViewMetadata

    public class LessonViewMetadata //actually TeamViewMetaData
    {
        //public int TeamViewID { get; set; }
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "Username is required.")]
        public string UserID { get; set; }
        public int TeamID { get; set; }
        [Display(Name = "Date Viewed")]
        public Nullable<System.DateTime> DateViewed { get; set; }
    }

    [MetadataType(typeof(LessonViewMetadata))]
    public partial class LessonView { }
    #endregion
}
