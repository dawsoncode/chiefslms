﻿using ChiefsLMS.UI.MVC.Models;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace ChiefsLMS.UI.MVC.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            string body = string.Format("Name: {0}<br/>Email: {1}<br/>Subject: {2}<br/>Message:<br/>{3}",
               contact.Name,
               contact.Email,
               contact.Subject,
               contact.Message);

            Debug.WriteLine(body);

            //2. Create and configure the mail message

            MailMessage msg = new MailMessage("no-reply@dawsonhiggins.com", //From address must be an email on hosting account,
                "dawson.higgins24@outlook.com", //To address where you want the email to go
                contact.Subject, //subject of message
                body);

            msg.IsBodyHtml = true;

            Debug.WriteLine(msg.From);

            //3. Additional mail message configurations

            msg.CC.Add("dawson.higgins24@outlook.com"); //Adds a CC email address to message
            //msg.BCC.ADD("student@domain.com") Adds a BCC email address to message
            msg.Priority = MailPriority.High;

            //4. Create and configure the SMTP Client

            SmtpClient client = new SmtpClient("mail.dawsonhiggins.com");
            client.Credentials = new NetworkCredential("no-reply@dawsonhiggins.com", "P@ssw0rd");

            //5. Attempt to send the message.

            if (ModelState.IsValid)
            {
                using (client)
                {
                    try
                    {
                        client.Send(msg);
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was an error sending your email. Please try again.";
                        return View();
                    }//end try/catch
                }//end using

                //send the user to the contact confirmation view and pass through the object with the contact information
                //in the event that an error is thrown, this code will not run. That is because the Return View() in the catch

                return View("ContactConfirmation", contact);

            }//end if

            return View();
        }
    }
}
