﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ChiefsLMS.DATA.EF;
using Microsoft.AspNet.Identity;

namespace ChiefsLMS.UI.MVC.Controllers
{
    public class DivisionsController : Controller
    {
        private ChiefsLMSEntities db = new ChiefsLMSEntities();

        // GET: Divisions
        public ActionResult Index()
        {
            return View(db.Divisions.ToList());
        }

        // GET: Divisions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Division division = db.Divisions.Find(id);
            if (division == null)
            {
                return HttpNotFound();
            }

            string userID = User.Identity.GetUserId();
            var teams = db.Teams.Where(x => x.DivisionID == division.DivisionID);
            var completions = db.LessonViews.Where(x => x.UserID == userID && x.Team.DivisionID == division.DivisionID).Select(x => x.TeamID).ToList();
            if (User.IsInRole("Player"))
            {
                List<int> divisionLessons = new List<int>();
                List<int> viewedLessons = new List<int>();

                foreach (var item in teams)
                {
                    divisionLessons.Add(item.TeamID);
                }

                var divisionLessonViews = db.LessonViews.Where(x => x.UserID == userID && x.Team.DivisionID == division.DivisionID);
                foreach (var item in divisionLessonViews)
                {
                    viewedLessons.Add(item.TeamID);
                }

                viewedLessons.Sort();
                divisionLessons.Sort();

                if (divisionLessons.SequenceEqual(viewedLessons))
                {
                    CourseCompletion completion = new CourseCompletion()
                    {
                        DivisionID = division.DivisionID,
                        UserID = userID,
                        DateCompleted = DateTime.Now
                    };
                    List<int> courseCompletions = new List<int>();
                    foreach (var item in teams)
                    {
                        courseCompletions.Add(item.DivisionID);
                    }

                    var userCompletions = db.CourseCompletions.Where(x => x.UserID == userID).ToList();
                    List<int> courseList = new List<int>();
                    foreach (var item in userCompletions)
                    {
                        courseList.Add(item.DivisionID);
                    }

                    if (!(courseList.Contains(completion.DivisionID)))
                    {



                        db.CourseCompletions.Add(completion);
                        db.SaveChanges();

                    }
                }




             }


            return View(division);
        }

        // GET: Divisions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Divisions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DivisionID,DivisionName,Description,IsActive")] Division division)
        {
            if (ModelState.IsValid)
            {
                db.Divisions.Add(division);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(division);
        }

        // GET: Divisions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Division division = db.Divisions.Find(id);
            if (division == null)
            {
                return HttpNotFound();
            }
            return View(division);
        }

        // POST: Divisions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DivisionID,DivisionName,Description,IsActive")] Division division)
        {
            if (ModelState.IsValid)
            {
                db.Entry(division).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(division);
        }

        // GET: Divisions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Division division = db.Divisions.Find(id);
            if (division == null)
            {
                return HttpNotFound();
            }
            return View(division);
        }

        // POST: Divisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Division division = db.Divisions.Find(id);
            db.Divisions.Remove(division);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
