﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ChiefsLMS.DATA.EF;
using Microsoft.AspNet.Identity;

namespace ChiefsLMS.UI.MVC.Controllers
{
    public class TeamsController : Controller
    {
        private ChiefsLMSEntities db = new ChiefsLMSEntities();

        // GET: Teams
        public ActionResult Index()
        {
            var teams = db.Teams.Include(t => t.Division);
            return View(teams.ToList());
        }

        // GET: Teams/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }

            if (User.IsInRole("Player"))
            {
                string userID = User.Identity.GetUserId();
                LessonView view = new LessonView()
                {
                    DateViewed = DateTime.Now,
                    TeamID = team.TeamID,
                    UserID = userID
                };
                var userViews = db.LessonViews.Where(x => x.UserID == view.UserID).ToList();
                List<int> userLessonViews = new List<int>();
                foreach (var item in userViews)
                {
                    userLessonViews.Add(item.TeamID);
                }

                if (!userLessonViews.Contains(view.TeamID))
                {
                    db.LessonViews.Add(view);
                    db.SaveChanges();
                }


            }

            return View(team);
        }

        // GET: Teams/Create
        public ActionResult Create()
        {
            ViewBag.DivisionID = new SelectList(db.Divisions, "DivisionID", "DivisionName");
            return View();
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TeamID,TeamName,DivisionID,Introduction,VideoURL,PdfFileName,IsActive")] Team team, HttpPostedFileBase coverPDF)
        {
            if (ModelState.IsValid)
            {

                #region Process File Upload

                //create a default image name
                string teamPDF = "NoPDF.pdf";

                //Check file upload for null
                if (coverPDF != null)
                {
                    teamPDF = coverPDF.FileName;

                    //create a variable for the file extension
                    string ext = teamPDF.Substring(teamPDF.LastIndexOf("."));

                    //list of valid extensions
                    string validExt = ".pdf" ;

                    //check our extension against the list
                    if (validExt == ext.ToLower())
                    {
                        //(optional) make file name unique
                        teamPDF = team.TeamName + DateTime.Now.ToString("yyyyMMddhhmmss") + ext;

                        //save image to server
                        coverPDF.SaveAs(Server.MapPath("~/Content/wines/images/PDF/" + teamPDF));
                    }
                    else
                    {
                        //use default image
                        teamPDF = "NoPDF.pdf";
                    }
                }

                //no matter what, persist the image to book object
                team.PdfFileName = teamPDF;
                #endregion

                db.Teams.Add(team);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DivisionID = new SelectList(db.Divisions, "DivisionID", "DivisionName", team.DivisionID);
            return View(team);
        }

        // GET: Teams/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            ViewBag.DivisionID = new SelectList(db.Divisions, "DivisionID", "DivisionName", team.DivisionID);
            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TeamID,TeamName,DivisionID,Introduction,VideoURL,PdfFileName,IsActive")] Team team, HttpPostedFileBase coverPDF)
        {
            if (ModelState.IsValid)
            {
                #region Process File Upload
                //no default image assignment -- the record has a property that defines the current image

                if (coverPDF != null)
                {
                    string teamPDF = coverPDF.FileName;

                    //Variable for the extension
                    string ext = teamPDF.Substring(teamPDF.LastIndexOf("."));

                    //valid extensions list
                    string validExt =  ".pdf";

                    //check ectension against the list
                    if (validExt == ext.ToLower())
                    {
                        //teamPDF = team.TeamName + DateTime.Now.ToString("yyyyMMddhhmmss") + ext;

                        //save to the server
                        coverPDF.SaveAs(Server.MapPath("~/Content/wines/images/PDF/" + teamPDF));

                        //delete the old image from the server
                        //if (System.IO.File.Exists(Server.MapPath("~/Content/wines/images/PDF/" + team.PdfFileName)) &&
                        //    team.PdfFileName.ToLower() != "nopdf.pdf")
                        //{
                        //    System.IO.File.Delete(Server.MapPath("~/Content/wines/images/PDF/" + team.PdfFileName));
                        //}

                        //save to db
                        team.PdfFileName = teamPDF;

                    }
                }

                #endregion

                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DivisionID = new SelectList(db.Divisions, "DivisionID", "DivisionName", team.DivisionID);
            return View(team);
        }

        // GET: Teams/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Team team = db.Teams.Find(id);
            db.Teams.Remove(team);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
